package com.androidexample.mvaish.cookies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void eatCookie(View view){
        TextView status = (TextView) findViewById(R.id.status_text_view);
        ImageView cookie = (ImageView) findViewById(R.id.cookie_icon);
        cookie.setImageResource(R.drawable.after_cookie);
        status.setText("I'm so full");
    }
}
