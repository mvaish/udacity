package com.androidexample.mvaish.justjava;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.zip.CheckedInputStream;

public class MainActivity extends AppCompatActivity {
    private final String TAG = "MainActivity";
    private int quantity = 2;
    private boolean addWhippedCreamChecked = false;
    private boolean addChocolateChecked = false;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        EditText nameText = (EditText) findViewById(R.id.name);
        name = nameText.getText().toString();
        String priceMessage = getResources().getString(R.string.no_order_yet);
        String[] topping = {getResources().getString(R.string.no), getResources().getString(R.string.no)};

        if (quantity > 0) {
            if (addWhippedCreamChecked) {
                topping[0] = getResources().getString(R.string.yes);
            }
            if (addChocolateChecked) {
                topping[1] = getResources().getString(R.string.yes);
            }
            int price = calculatePrice();
            priceMessage = createOrderSummary(name, price, topping);
        }

        //Creating intent to send the order via email
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        //Prepare Subject line
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.mail_subject, name));

        //Prepare message int he email body
        intent.putExtra(Intent.EXTRA_TEXT, priceMessage);

        //Check if the device has any app which will take care of this intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private String createOrderSummary(String name, int price, String[] topping) {
        String message = getResources().getString(R.string.customer_name, name) + "\n";
        message += getResources().getString(R.string.add_whipped_cream, topping[0]) + "\n";
        message += getResources().getString(R.string.add_chocolate, topping[1]) + "\n";
        message += getResources().getString(R.string.ordered_quantity, quantity) + "\n";
        message += getResources().getString(R.string.order_total, NumberFormat.getCurrencyInstance().format(price)) + "\n";
        message += getResources().getString(R.string.greetings);
        return message;
    }

    public void checkTopping(View view) {
        Log.v(TAG, "in checkTopping()");
        //Is this view checked?
        boolean checked = ((CheckBox) view).isChecked();
        //Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.chocolate_topping:
                if (checked) {
                    addChocolateChecked = true;
                } else {
                    addChocolateChecked = false;
                }
                break;
            case R.id.whipped_cream_topping:
                if (checked) {
                    addWhippedCreamChecked = true;
                } else {
                    addWhippedCreamChecked = false;
                }
                break;
        }
        Log.v(TAG, "Toppings checked" + ((CheckBox) view).getText() + ((CheckBox) view).isChecked());
    }

    /**
     * Calculates the price of the order.
     */
    private int calculatePrice() {
        int unitPrice = 5;
        if (addChocolateChecked) {
            unitPrice += 2;
        }
        if (addWhippedCreamChecked) {
            unitPrice += 1;
        }
        Log.v(TAG, "in calculatePrice()");
        Log.v(TAG, "new price is" + quantity * unitPrice);
        return quantity * unitPrice;
    }

    /**
     * This method is called when the plus button is clicked.
     */
    public void increment(View view) {
        Log.v(TAG, "in increment()");
        Log.v(TAG, "old quantity is" + quantity);
        if (quantity == 100) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.more_than_100), Toast.LENGTH_SHORT).show();
            return;
        }
        display(++quantity);
        Log.v(TAG, "new quantity is" + quantity);
    }

    /**
     * This method is called when the minus button is clicked.
     */
    public void decrement(View view) {
        Log.v(TAG, "in decrement()");
        Log.v(TAG, "old quantity is" + quantity);
        if (quantity == 1) {
            Toast.makeText(MainActivity.this, getResources().getString(R.string.less_than_0), Toast.LENGTH_SHORT).show();
            return;
        }
        quantity -= 1;
        Log.v(TAG, "new quantity is" + quantity);
        display(quantity);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }
}
