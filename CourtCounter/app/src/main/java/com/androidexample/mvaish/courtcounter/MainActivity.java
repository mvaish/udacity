package com.androidexample.mvaish.courtcounter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int scoreA = 0;
    int scoreB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Displays the given score for Team B.
     */
    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    /**
     * Updates the score for Team A.
     */
    public void updateScoreTeamA(View view) {
        Button btn = (Button) findViewById(view.getId());
        String buttonText = btn.getText().toString();

        if (buttonText.contains("3")) {
            scoreA += 3;
        } else if (buttonText.contains("2")) {
            scoreA += 2;
        } else {
            scoreA += 1;
        }
        displayForTeamA(scoreA);
    }

    /**
     * Updates the score for Team B.
     */
    public void updateScoreTeamB(View view) {
        Button btn = (Button) findViewById(view.getId());
        String buttonText = btn.getText().toString();

        if (buttonText.contains("3")) {
            scoreB += 3;
        } else if (buttonText.contains("2")) {
            scoreB += 2;
        } else {
            scoreB += 1;
        }
        displayForTeamB(scoreB);
    }
    /**
     * Updates the score for Team B.
     */
    public void resetScore(View view) {
        scoreA = 0;
        scoreB = 0;
        displayForTeamA(scoreA);
        displayForTeamB(scoreB);
    }
}
